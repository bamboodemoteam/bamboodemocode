/**
 * 
 */
package com.jcg.examples.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.jcg.examples.controller.LoginController;
import com.jcg.examples.viewBean.LoginBean;

/**
 * @author p0121093
 *
 */
public class LoginControllerTest {

	@Autowired
	LoginBean loginBean=new LoginBean();
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		loginBean.setUsername("sara");
		loginBean.setPassword("sara1");
	}
	
	
	

	/**
	 * Test method for {@link com.jcg.examples.controller.LoginController#executeLogin(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.jcg.examples.viewBean.LoginBean)}.
	 */
	@Test
	public void testExecuteLogin() {
		
		assertEquals(loginBean.getUsername(),loginBean.getUsername());
		
	}
	
	@Test
	public void testUserNameEmpty() {
		assertFalse(loginBean.getUsername().isEmpty());
	}
	
	@Test
	public void testPasswordEmpty() {
		assertFalse(loginBean.getPassword().isEmpty());
	}
	
	@Test
	public void testUserNameLength() {
		assertEquals(4,loginBean.getUsername().length());
	}
	
	@Test
	public void testPasswordLength() {
		assertEquals(5, loginBean.getPassword().length());
	}
	

}
