/**
 * 
 */
package com.jcg.examples.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.jcg.examples.viewBean.LoginBean;
import com.jcg.examples.viewBean.ReservationBean;

/**
 * @author p0121093
 *
 */
public class ReservationControllerTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		bean.setMsisdn("123");
	}
	
	
	ReservationBean bean=new ReservationBean();

	/**
	 * Test method for {@link com.jcg.examples.controller.ReservationController#reserveNumber(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.jcg.examples.viewBean.ReservationBean)}.
	 */
	@Test
	public void testReserveNumber() {
		assertNotNull(bean.getMsisdn());
	}
	
	@Test
	public void testMSISDNLength() {
		assertEquals(3,bean.getMsisdn().length());
	}

}
