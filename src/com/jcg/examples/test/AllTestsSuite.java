package com.jcg.examples.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ LoginControllerTest.class, ReservationControllerTest.class })
public class AllTestsSuite {

}
